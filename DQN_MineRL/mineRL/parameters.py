param = {

    "nb_episodes_training" : 10,
    "nb_episodes_demo" : 5,
    "duration_episodes" : 500,
    "seed" : 143,
    "epsilon_start" : 1,
    "epsilon_decay" : 0.995,
    "epsilon_end" : 0.02,
    "learning_rate": 0.001,
    "gamma": 0.99,
    "memory_size" : 5000,
    "min_memory_size" : 100,
    "target_mode": True,
    "alpha" : 0.03
}