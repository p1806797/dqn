from copy import deepcopy

import gym
import torch
import torch.nn as nn
import random
import numpy as np

from parameters import param

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

torch.manual_seed(param["seed"])
np.random.seed(param["seed"])
random.seed(param["seed"])

class Memory:

    def __init__(self, _size):
        self.buffer = []
        self.size = _size

    def add_interaction(self, state0, action, state1, reward, done):
        interaction = (state0, action, state1, reward, done)
        self.buffer.append(interaction)
        if len(self.buffer) > self.size:
            self.buffer.pop(0)

    def get_random_interactions(self, n):
        interactions = random.sample(self.buffer, n)

        s = np.asarray([exp[0] for exp in interactions]).squeeze(1)
        a = np.asarray([exp[1] for exp in interactions])
        sn = np.asarray([exp[2] for exp in interactions])
        r = np.asarray([exp[3] for exp in interactions])
        d = np.asarray([exp[4] for exp in interactions])

        s_t = torch.as_tensor(s, dtype=torch.float32, device=device)
        a_t = torch.as_tensor(a, dtype=torch.int64, device=device)#.unsqueeze(-1)
        sn_t = torch.as_tensor(sn, dtype=torch.float32, device=device).squeeze(0)
        r_t = torch.as_tensor(r, dtype=torch.float32, device=device).unsqueeze(-1)
        d_t = torch.as_tensor(d, dtype=torch.float32, device=device).unsqueeze(-1)

        return s_t, a_t, sn_t, r_t, d_t


class NeuralNetwork(nn.Module):

    def __init__(self, input_shape, hidden_size, hidden_size2, output_size):
        super(NeuralNetwork, self).__init__()

        # instanciation d'un réseaux de convolution
        self.conv = nn.Sequential(
            nn.Conv2d(in_channels=1, out_channels=32, kernel_size=(6, 6), stride=(2, 2), padding=(1, 1), device=device),
            nn.ReLU(),
            nn.Conv2d(in_channels=32, out_channels=64, kernel_size=(6, 6), stride=(2, 2), padding=(1, 1), device=device),
            nn.ReLU(),
            nn.Conv2d(in_channels=64, out_channels=64, kernel_size=(4, 4), stride=(2, 2), padding=(1, 1), device=device),
            nn.ReLU()
        )
        # intanciation d'un réseau de layer fully connected
        self.fc = nn.Sequential(
            nn.Linear(1024, 512, device=device),
            nn.ReLU(),
            nn.Linear(512, output_size, device=device)
        )

    def forward(self, observation):
        conv_out = torch.flatten(self.conv(observation))
        return self.fc(conv_out)


class DQNAgent:

    def __init__(self, env, demo=False):

        self.sample_size = 1
        self.action_size = 3
        self.state_size = env.observation_space.shape[0]
        self.input_shape = (1,40,40)
        self.network = NeuralNetwork(self.input_shape, 64, 64, self.action_size).to(device)
        self.target_network = NeuralNetwork(self.input_shape, 64, 64, self.action_size).to(device)
        self.target_network.load_state_dict(self.network.state_dict())

        self.epsilon = param["epsilon_start"]
        if demo:
            self.epsilon = 0
        self.epsilon_decay = param["epsilon_decay"]
        self.epsilon_end = param["epsilon_end"]
        self.lr = param["learning_rate"]
        self.gamma = param["gamma"]
        self.memory = Memory(param["memory_size"])
        self.min_memory_size = param["min_memory_size"]

        self.loss_fn = torch.nn.MSELoss()
        self.optimizer = torch.optim.Adam(self.network.parameters(), lr=self.lr)
        self.target_mode = param["target_mode"]
        self.alpha = param["alpha"]

    def save(self, path):
        torch.save(self.network.state_dict(), path)

    def load(self, path):
        self.network.load_state_dict(torch.load(path))
        self.network.eval()


    def get_action(self, observation):

        rand = random.random()
        if rand < self.epsilon:
            action = random.choice(range(self.action_size))
        else:
            obs = torch.Tensor(observation)
            qvalues = self.network.forward(obs.to(device))
            action = int(
                ((qvalues == torch.max(qvalues)).nonzero(as_tuple=True)[0])[0])  # get the index of the highest Q value

        if self.epsilon > self.epsilon_end:
            self.epsilon = self.epsilon * self.epsilon_decay
        if self.epsilon < self.epsilon_end:
             self.epsilon = self.epsilon_end
        return action

    def train(self):
        s, a, ns, r, d = self.memory.get_random_interactions(self.sample_size)


        q_values = self.network(s)
        action_q_values = torch.gather(input=q_values, dim=0, index=a)


        # targets
        target_q_values = self.target_network(ns)
        max_target_q_values = target_q_values.max(dim=0, keepdim=True)[0]
        targets = (r + self.gamma * (1 - d) * max_target_q_values).squeeze(0)


        # calcul de la loss
        loss = self.loss_fn(action_q_values, targets)

        # descente de gradient
        self.optimizer.zero_grad()
        loss.backward()
        self.optimizer.step()

        if self.target_mode:
            temp_dict = deepcopy(self.target_network.state_dict())
            for k, v in self.target_network.state_dict().items():
                # if "weight" in k:
                temp_dict[k] = (1 - self.alpha) * self.target_network.state_dict()[k] + self.alpha * \
                               self.network.state_dict()[k]
            self.target_network.load_state_dict(temp_dict)

        return loss.item()