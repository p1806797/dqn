import gym
import numpy as np
import torch
from matplotlib import pyplot as plt

from wrapper import ObservationWrapper
from dqn_agent import DQNAgent
from custom_environments import *
from parameters import param

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
print('device : ', device)

NB_EPISODES = param["nb_episodes_training"]
DURATION_EPISODE = param["duration_episodes"]
SEED = param["seed"]
LOG_FREQUENCY = 1
RECORD = False
RENDER = True
SAVE = True

# fonction de convertion des action de leur format entier en a format compréhensible par l'environement
def mapAction(action, env):
    action_dict = env.action_space.noop()
    # ajout de la caméra au dictionaire d'action (car nécessaire)
    action_dict['camera'] = [0, 0]
    if action == 1:
        action_dict['left'] = 1
    elif action == 0:
        action_dict['right'] = 1
    elif action == 2:
        action_dict['attack'] = 1

    return action_dict

# meme fonctionement que sur le DQN carpool
def run_episodes(env, agent):

    episodes = []
    rewards = []
    average_rewards = []
    for i_episode in range(NB_EPISODES):
        obs = env.reset()
        cumulated_reward = 0
        for t in range(DURATION_EPISODE):
            if RENDER:
                env.render()
            action = agent.get_action(obs)
            action_dict = mapAction(action, env)
            new_obs, reward, done, _ = env.step(action_dict)
            cumulated_reward += reward
            agent.memory.add_interaction(obs, action, new_obs, reward, done)
            obs = new_obs
            if len(agent.memory.buffer) >= agent.sample_size:
                agent.train()
            if done:
                break
        average_rewards.append(cumulated_reward)
        if len(average_rewards) > 10:
            average_rewards.pop(0)
        print(
            f"Episode {i_episode}/{NB_EPISODES} | {t + 1} timesteps | cumulated_rewards = {cumulated_reward}| epsilon = {agent.epsilon} | memory_size = {len(agent.memory.buffer)}")
        episodes.append(i_episode)
        rewards.append(cumulated_reward)

    print("Learning finished")
    return episodes, rewards

def plot_episodes(episodes, rewards, path):
    plt.figure()
    plt.plot(episodes, rewards)
    plt.title('Evolution du cumul des rewards par épisode')
    plt.xlabel('episode')
    plt.ylabel('rewards')
    plt.savefig(path)
    plt.show()


def main():

    env = ObservationWrapper(gym.make("Mineline-v0"))
    env.seed(param["seed"])

    agent = DQNAgent(env)

    episodes, rewards = run_episodes(env, agent)
    plot_episodes(episodes, rewards, "results/graph_training.png")

    if SAVE:
        agent.save("results/network.pt")


if __name__ == "__main__":
    main()