import gym
from gym import Space
from gym.wrappers.monitoring.video_recorder import VideoRecorder
import torch
import random
import ffmpeg
import matplotlib.pyplot as plt

from wrapper import ObservationWrapper
from DQN_MineRL.mineRL.dqn_agent import DQNAgent
from training import plot_episodes
from training import mapAction
from parameters import param


NB_EPISODES = param["nb_episodes_demo"]
DURATION_EPISODE = param["duration_episodes"]
RECORD = True
RENDER = True

def demo(env, agent):

    video_recorder = VideoRecorder(env, path="results/replay_demo.mp4", enabled = RECORD)
    episodes = []
    rewards = []
    for i_episode in range(NB_EPISODES):
        observation = env.reset()
        cumulated_reward = 0

        for t in range(DURATION_EPISODE):
            if RENDER:
                env.render()
            video_recorder.capture_frame()
            action = agent.get_action(observation)
            action_dict = mapAction(action, env)
            new_observation, reward, done, _ = env.step(action_dict)
            cumulated_reward += reward
            observation = new_observation
            if done:
                break
        print(f"Episode {i_episode}/{NB_EPISODES} | {t + 1} timesteps | cumulated_rewards = {cumulated_reward}")
        episodes.append(i_episode)
        rewards.append(cumulated_reward)
    video_recorder.close()
    video_recorder.enabled = False
    print("Demo finished")
    return episodes, rewards

def main():

    env = ObservationWrapper(gym.make("Mineline-v0"))
    env.seed(param["seed"])

    agent = DQNAgent(env, demo=True)
    agent.load("results/network.pt")

    episodes, rewards = demo(env, agent)
    plot_episodes(episodes, rewards, "results/graph_demo.png")

if __name__ == "__main__":
    main()