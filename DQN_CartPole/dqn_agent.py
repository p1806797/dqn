from copy import deepcopy

import gym
import torch
import torch.nn as nn
import random
import numpy as np

from parameters import param

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

#définie une seed pour tout les random qui seront utilisés
torch.manual_seed(param["seed"])
np.random.seed(param["seed"])
random.seed(param["seed"])

# classe servant a gérer la mémoire des intéractions passées
class Memory:

    def __init__(self, _size):
        self.buffer = []
        self.size = _size

    # ajoute une intéraction a la mémoire
    def add_interaction(self, state0, action, state1, reward, done):
        interaction = (state0, action, state1, reward, done)
        self.buffer.append(interaction)
        if len(self.buffer) > self.size:
            self.buffer.pop(0)

    # fonction qui retourne un sample de la mémoire des intéractions
    def get_random_interactions(self, n):
        # retourne n intéractions parmi la mémoire
        interactions = random.sample(self.buffer, n)

        # création de np array des differents élements d'un sample
        s = np.asarray([exp[0] for exp in interactions])
        a = np.asarray([exp[1] for exp in interactions])
        sn = np.asarray([exp[2] for exp in interactions])
        r = np.asarray([exp[3] for exp in interactions])
        d = np.asarray([exp[4] for exp in interactions])

        # conversion de ses array en tensor
        s_t = torch.as_tensor(s, dtype=torch.float32, device=device)
        a_t = torch.as_tensor(a, dtype=torch.int64, device=device).unsqueeze(-1)
        sn_t = torch.as_tensor(sn, dtype=torch.float32, device=device)
        r_t = torch.as_tensor(r, dtype=torch.float32, device=device).unsqueeze(-1)
        d_t = torch.as_tensor(d, dtype=torch.float32, device=device).unsqueeze(-1)

        # retourne un tensor de taille définie pour les états, action, états suivant, reward
        # et fin d'itération

        return s_t, a_t, sn_t, r_t, d_t

# définition du réseau de neurones
class NeuralNetwork(nn.Module):

    # spécification du layout du réseau
    def __init__(self, input_size, nb_hidden_layers, hidden_size1, hidden_size2,  output_size):
        super(NeuralNetwork, self).__init__()
        self.nb_hidden_layers = nb_hidden_layers
        self.l1 = nn.Linear(input_size, hidden_size1, device=device)
        if self.nb_hidden_layers == 2:
            self.l2 = nn.Linear(hidden_size1, hidden_size2, device=device)
            self.l3 = nn.Linear(hidden_size2, output_size, device=device)
        else:
            self.l2 = nn.Linear(hidden_size1, output_size, device=device)
        self.activation = nn.ReLU()

    # spécification de l'ordre "d'éxécution" du réseau
    def forward(self, observation):
        out = self.l1(observation)
        out = self.activation(out)
        if self.nb_hidden_layers == 2:
            out = self.l2(out)
            out = self.activation(out)
            out = self.l3(out)
        else:
            out = self.l2(out)
        return out


class DQNAgent:

    def __init__(self, env, demo=False, p=param):

        self.target_mode = p["target_mode"]
        # stocke le nombre d'actions possible dans l'environement
        self.action_size = env.action_space.n
        self.state_size = int(np.prod(env.observation_space.shape))

        # creation du réseau de neurones de prédiction
        self.network = NeuralNetwork(self.state_size, p["nb_hidden_layers"], p['hidden_layer_size1'], p['hidden_layer_size2'], self.action_size).to(device)
        # creation du réseau de neurones pour la cible
        self.target_network = NeuralNetwork(self.state_size, p["nb_hidden_layers"], p['hidden_layer_size1'], p['hidden_layer_size2'], self.action_size).to(device)
        # copie des poids du réseau de neurones de prédiction dans le réseau de la cible
        self.target_network.load_state_dict(self.network.state_dict())

        # instanciation des paramètres
        # epsilon mit a 0 si en mode demo ou autrement a la valeur spécifié en paramètre
        self.epsilon = 0 if demo else p["epsilon_start"]
        # valeur par laquelle sera multiplié epsilon a chaque itération
        self.epsilon_decay = p["epsilon_decay"]
        # valeur minimale d'epsilon
        self.epsilon_end = p["epsilon_end"]
        # learning rate
        self.lr = p["learning_rate"]
        self.gamma = p["gamma"]

        # taille du batch
        self.sample_size = p["sample_size"]
        # taille de la mémoire
        self.memory = Memory(p["memory_size"])
        # taille minimum de la mémoire avant de commencer l'entrainement
        self.min_memory_size = p["min_memory_size"]
        self.alpha = p["alpha"]

        self.loss_fn = torch.nn.MSELoss()
        self.optimizer = torch.optim.Adam(self.network.parameters(), lr=self.lr)

    # fonction pour sauvegarder le réseau de neurones de prédiction
    def save(self, path):
        torch.save(self.network.state_dict(), path)

    # fonction pour charger le réseau de neurones de prédiction
    def load(self, path):
        self.network.load_state_dict(torch.load(path))
        self.network.eval()

    # retourne l'action optimale choisi par le réseau de neurones de prédiction
    def get_action(self, observation):
        # tire aléatoirement une action si random inférieur a epsilon
        rand = random.random()
        if rand < self.epsilon:
            action = random.choice(range(self.action_size))
        else:
            # choisi l'action ayant la plus haute Q-valeur
            obs = torch.Tensor(observation)
            qvalues = self.network.forward(obs.to(device))
            action = int(
                ((qvalues == torch.max(qvalues)).nonzero(as_tuple=True)[0])[0])  # get the index of the highest Q value

        # réduit epsilon tant qu'il est supérieur a la limite basse fixé
        if self.epsilon > self.epsilon_end:
            self.epsilon = self.epsilon * self.epsilon_decay
        if self.epsilon < self.epsilon_end:
            self.epsilon = self.epsilon_end
        return action

    def train(self):
        # récupère le batch d'état, action, état suivant, reward, fin de l'environement
        s, a, ns, r, d = self.memory.get_random_interactions(self.sample_size)

        # calcul du terme prédiction
        q_values = self.network(s)
        action_q_values = torch.gather(input=q_values, dim=1, index=a)

        # calcul du terme cible
        target_q_values = self.target_network(ns) if self.target_mode else self.network(ns)
        max_target_q_values = target_q_values.max(dim=1, keepdim=True)[0]
        targets = r + self.gamma * (1 - d) * max_target_q_values

        # calcul de la loss
        loss = self.loss_fn(action_q_values, targets)

        # descente de gradient
        self.optimizer.zero_grad()
        loss.backward()
        self.optimizer.step()

        # mise a jour du réseau de neurones cible
        if self.target_mode:
            temp_dict = deepcopy(self.target_network.state_dict())
            for k, v in self.target_network.state_dict().items():
                temp_dict[k] = (1 - self.alpha) * self.target_network.state_dict()[k] + self.alpha * \
                               self.network.state_dict()[k]
            self.target_network.load_state_dict(temp_dict)

        return loss.item()
