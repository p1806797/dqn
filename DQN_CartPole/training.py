import gym
from gym.wrappers.monitoring.video_recorder import VideoRecorder
import torch
import matplotlib.pyplot as plt
import numpy as np

# from random_agent import RandomAgent
from DQN_CartPole.dqn_agent import DQNAgent
from parameters import param

NB_EPISODES = param["nb_episodes_training"]
DURATION_EPISODE = param["duration_episodes"]
TARGET_AVERAGE_SCORE = param["target_average_score"]
SEED = param["seed"]
LOG_FREQUENCY = param["log_frequency"]
# définie si l'entrainement sera enregistré (vidéo)
RECORD = False
# définie si l'environement sera visible
RENDER = False
# définie si le DQN sera sauvegardé
SAVE = False

# permet d'éxécuter le code sur une carte graphique si une compatible est présente
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
print('device : ', device)

def run_episodes(env, agent):

    episodes = []
    rewards = []
    average_reward = []
    # entraine les réseaux pour NB épisode * duration épisode
    for i_episode in range(NB_EPISODES):
        observation = env.reset()
        cumulated_reward = 0
        for t in range(DURATION_EPISODE):
            if RENDER:
                env.render()
            # retourne l'action choisit pour le DQN
            action = agent.get_action(observation)
            # met a jour l'état, la reward et si l'épisode est finit
            new_observation, reward, done, info = env.step(action)  # take a random action
            # ajoute 1 au score car il survécu une unité de temps suplémentaire
            cumulated_reward += reward
            # ajout une intéraction dans la mémoire
            agent.memory.add_interaction(observation, action, new_observation, reward, done)
            observation = new_observation
            # entraine les réseaux seulement si la mémoire est assez grande
            if len(agent.memory.buffer) >= agent.sample_size:
                agent.train()
            # si l'environement retourne que l'itération est finit, fin de l'entrainement sur cette itération
            if done:
                break
        average_reward.append(cumulated_reward)
        # calcul et mise a jour de la reward moyenne sur 100 itérations
        if len(average_reward) > 100:
            average_reward.pop(0)
        if i_episode % LOG_FREQUENCY == 0:
            print(
                f"Episode {i_episode}/{NB_EPISODES} | {t + 1} timesteps | average_rewards = {np.mean(average_reward)}| epsilon = {agent.epsilon} | memory_size = {len(agent.memory.buffer)}")
        episodes.append(i_episode)
        rewards.append(cumulated_reward)
        # stop l'entrainement si un score moyen minimum a été atteint
        if np.mean(average_reward) > TARGET_AVERAGE_SCORE:
            print(f"Average reward of the 100 last episodes = {np.mean(average_reward)} > {TARGET_AVERAGE_SCORE}, Learning finished")
            return episodes, rewards
    env.close()
    print("Learning finished")
    return episodes, rewards


def plot_episodes(episodes, rewards, path):
    plt.figure()
    plt.plot(episodes, rewards)
    plt.title('Evolution du cumul des rewards par épisode')
    plt.xlabel('episode')
    plt.ylabel('rewards')
    plt.savefig(path)
    plt.show()


def main():
    # creation de l'environement
    env = gym.make(param["env"])
    # attribution d'une seed d'aléatoire
    env.seed(param["seed"])
    env.action_space.seed(param["seed"])

    # creation de l'agent (le DQN)
    agent = DQNAgent(env)
    # entrainement
    episodes, rewards = run_episodes(env, agent)
    plot_episodes(episodes, rewards, "results/graph_training.png")

    # sauvegarde des poids du réseau si spécifié
    if SAVE:
        agent.save("results/network.pt")

if __name__ == "__main__":
    main()
