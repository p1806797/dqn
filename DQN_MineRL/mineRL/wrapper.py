import gym
import cv2
import numpy as np

# wrappers pour modifier le format de l'observation fournit par l'enviromenent
class ObservationWrapper(gym.ObservationWrapper):
    def __init__(self, env):
        super(ObservationWrapper, self).__init__(env)
        self.observation_space = gym.spaces.Box(low=0, high=255, shape=(40, 40), dtype=float)

    def observation(self, obs):
        # convertit l'image en noir et blanc
        gray = cv2.cvtColor(obs['pov'], cv2.COLOR_BGR2GRAY)
        # change la taille de l'image a 40,40
        img = cv2.resize(gray, (40, 40))
        # normalisation de l'image
        img = img / 255.0
        return [[img]]
