# Projet DQN, OpenAI Gym et Pytorch
## Yohan Michelland, Victor Favre

### Installation des dépendances

Aller à la racine du repertoire et exécuter la commande :``pip install requirements.txt``

### Architecture du code
Les sources sont divisées en deux partie structurées de manière similaires:
- DQN_CartPole contient les parties 1 et 2 du TP
- DQN_MineRL contient la partie 3 du TP

Exécuter ``training.py`` pour entraîner un agent, puis ``demo.py`` pour enregistrer une démonstration.
Après cela, le dossier ```results``` contiendra :
- les poids du réseau entrainé : ```network.pt```
- un graphe représentant l'évolution de la récompense  au cours de l'entrainement : ```graph_training.png```
- un graphe représentant l'évolution de la récompense au cours de la démo : ``graph_demo.png``
- une vidéo de la démonstration : ``replay_demo.mp4``


#### L'ensemble des hyper-paramètres sont modifiables dans *parameters.py*. En voici la liste :

- **env** : environnement parmis ceux fourni par Gym
- **nb_episodes_training** : nombre d'épisodes maximaux avant l'arrêt de l'entraînement
- **nb_episodes_demo** : nombres d'épisodes avant arrêt de la démonstration
- **duration_episodes** : temps limite avant l'arrêt d'un épisode et le reset de l'environnement
- **target_average_score** : récompense cumulée moyenne sur 100 épisodes à atteindre pour arrêter l'entrainement
- **seed** : instance de rng
- **target_mode** : Si a True, ajout d'un target network à l'agent
- **nb_hidden_layers** : nombre de couches cachées dans les réseaux de neurones
- **hidden_layer_size1** : taille de la première couche cachée
- **hidden_layer_size2** : taille de la deuxième couche cachée, si elle existe
- **epsilon_start** : valeur de départ du ratio d'exploration epsilon
- **epsilon_decay** : valeur d'atténuation de epsilon a chaque iteration
- **epsilon_end** : valeur minimale du epsilon, à partir de laquelle l'atténuation n'a plus lieu
- **learning_rate** : taux d'apprentissage de l'optimiseur Adam
- **gamma** : taux d'atténuation dans le calcul de la cible (équation de Bellman)
- **sample_size** : nombre d'interactions traitées lors d'un cycle d'apprentissage
- **memory_size** : taille maximal du buffer, une fois dépassé le premier élément est retiré
- **min_memory_size** : taille nécessaire au buffer pour commencer l'apprentissage
- **alpha** : coefficient utilisé dans la formule de mise à jour du target network
- **log_frequency** : nombre d'épisodes avant un nouveau print dans le terminal, pendant l'apprentissage

### Le dossier *experiences* contient nos meilleurs résultats, ainsi que leurs paramètres. Tout est expliqué dans *rapport.pdf*
- Pour reproduire une expérience, copier le fichier ``parameters.py`` dans ``DQN_CartPole``, puis exécuter ``training.py`` puis ``demo.py``
- Pour tester une démonstration sur un agent pré-entrainé, copié le fichier ``network.pt`` contenant les poids du réseau dans le dossier ``results``, puis executer ``demo.py``


